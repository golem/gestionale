#ifndef PIN_H
#define PIN_H

#include <QDialog>
#include <QtSql>
#include <QMessageBox>
#include <QSettings>
#include "db.h"
// #include "status.h"
// #include "fullQuery.h"

namespace Ui {
class Pin;
}

class Pin : public QDialog
{
    Q_OBJECT

public:
    explicit Pin(QWidget *parent = nullptr);
    ~Pin();

private slots:
    void on_pushButtonOk_clicked();

    void on_pushButtonEsci_clicked();

private:
    Ui::Pin *ui;
};

#endif // PIN_H
