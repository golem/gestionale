#include "Pin.h"
#include "ui_Pin.h"
#include "EditWindow.h"

Pin::Pin(QWidget *parent) : QDialog(parent), ui(new Ui::Pin) {
    ui->setupUi(this);

    // Imposta la dialog come non ingrandibile/riducibile
    this->setFixedSize(this->size());

    if (!QSqlDatabase::drivers().contains("QMYSQL")) {
        QMessageBox::critical(this, "Nicolodi", "L'applicazione necessita del driver MYSQL");
    }

    ui->lineEditPin->setFocus();
}

Pin::~Pin()
{
    delete ui;
}

void Pin::on_pushButtonOk_clicked()
{
    QSqlQuery query;
    int idSocio = 0;
    bool questionarioAbilitato;
    QString pin = ui->lineEditPin->text();

    if (db.open()) {
        query = QSqlQuery(db);
        query.prepare("SELECT id, abilitaQuestionario FROM socio WHERE pin = :pin");
        query.bindValue(":pin", QString("%1").arg(pin));
        if (!query.exec()) {
            QMessageBox::critical(this, "Nicolodi", "Errore nella verifica del pin, verificare query.");
        } else {
            if (query.size() == 0) {
                QMessageBox::warning(this, "Nicolodi", "Pin errato!");
            } else if (query.size() > 1) {
                QMessageBox::warning(this, "Nicolodi", "Pin non univoco!");
            } else {
                query.first();
                questionarioAbilitato = query.value(1).toBool();
                if (!questionarioAbilitato) {
                    QMessageBox::information(this, "Nicolodi", QString("Questionario non abilitato per PIN %1").arg(pin));
                } else {
                    idSocio = query.value(0).toInt();
                    // QMessageBox::information(this, "Nicolodi", QString("Il socio ha id %1").arg(idSocio));
                }
            }
        }
        // Pulizia oggetti db
        // Per rilasciare le risorse
        // Qui come funziona? Di solito le risorse tipo le connessioni
        // sono risorse preziose che si devono aprire, usare per il minor
        // tempo possibile e quindi chiudere
        query.clear();
        //db.close();
    } else {
        QMessageBox::critical(this, "Nicolodi", "Impossibile connettersi al database, verificare impostazioni.");
    }

    ui->lineEditPin->setText("");
    ui->lineEditPin->setFocus();

    // Se ho scelto un socio valido proseguo con l'apertura
    // delle editwindow
    // Un socio e' valido se ha idSocio maggiore di zero
    if (idSocio > 0) {
        EditWindow *ew = new EditWindow(idSocio, this);
        ew->setAttribute(Qt::WA_DeleteOnClose);
        ew->setWindowModality(Qt::ApplicationModal);
        if (!ew->doNotShow) {
            ew->show();
        }
    }
}

void Pin::on_pushButtonEsci_clicked()
{
    QApplication::exit();
}
