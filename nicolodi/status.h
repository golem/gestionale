#ifndef STATUS_H
#define STATUS_H

#include <QStatusBar>
#include <QString>

#include "Pin.h"

namespace Ui {

    enum StatusType {
        INFO,
        SUCCESS,
        WARNING,
        ERROR
    };

    void status(StatusType type, QString message);

}

#endif
