#include "db.h"

QSqlDatabase db;

void connectDatabase() {
    /* retrieve settings for this application, group database */
    QSettings settings("it.linux.golem", "nicolodi");
    settings.beginGroup("database");

    /* open database and attach query */
    db = QSqlDatabase::addDatabase("QMYSQL");  /* QMYSQL <-- database driver */

    db.setHostName(settings.value("hostname").toString());
    db.setDatabaseName(settings.value("database").toString());
    db.setUserName(settings.value("username").toString());
    db.setPassword(settings.value("password").toString());

    // db.setHostName(settings.value("hostname","serverozzo.golem.linux.it").toString());
    // db.setDatabaseName(settings.value("database","golem_gestionale").toString());
    // db.setPort(settings.value("port", "3306").toInt());
    // db.setUserName(settings.value("username").toString());
    // db.setPassword(settings.value("password").toString());

    // QString hostname = settings.value("hostname","localhost").toString();
    // QString database = settings.value("database","golem_gestionale").toString();
    // int port = settings.value("port", "3306").toInt();
    // QString username = settings.value("username", "admin").toString();
    // QString password = settings.value("password", "password").toString();

    // db.setHostName(hostname);
    // db.setDatabaseName(database);
    // db.setPort(port);
    // db.setUserName(username);
    // db.setPassword(password);

    // query = QSqlQuery(db);
}
