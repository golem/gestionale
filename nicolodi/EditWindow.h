#ifndef EDITWINDOW_H
#define EDITWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QMessageBox>
#include <QtWidgets>
#include <QCloseEvent>
#include "db.h"

namespace Ui {
class EditWindow;
}

class EditWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit EditWindow(int idSocio, QWidget* parent = nullptr);
    ~EditWindow();

    bool doNotShow = false;

private slots:
    void on_pushButtonAnnulla_clicked();

    void on_pushButtonSalva_clicked();

private:
    Ui::EditWindow *ui;
    QSqlRelationalTableModel *model = nullptr;
    bool doNotShow_;
    int professioneIdx;
    int fonteIdx;

    void showError(const QSqlError &err);
    void closeEvent(QCloseEvent *event);

};

#endif // EDITWINDOW_H
