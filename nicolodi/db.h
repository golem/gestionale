#ifndef DB_H
#define DB_H

#include <QSettings>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>

extern QSqlDatabase db;
// extern QSqlQuery query;

extern void connectDatabase();

#endif // DB_H
