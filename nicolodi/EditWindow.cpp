#include "EditWindow.h"
#include "ui_EditWindow.h"

EditWindow::EditWindow(int idSocio, QWidget* parent) : QMainWindow(parent), ui(new Ui::EditWindow) {
    ui->setupUi(this);

    if (!db.open()) {
        showError(db.lastError());
        this->doNotShow = true;
        return;
    }

    // Crea il model
    model = new QSqlRelationalTableModel(this);
    model->setTable("socio");
    model->setFilter(QString("socio.id = %1").arg(idSocio));
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);

    // Indici dei campi in join
    professioneIdx = model->fieldIndex("professione");
    fonteIdx = model->fieldIndex("fonte");

    // La strategia di join da usare per le relazioni è LeftJoin
    model->setJoinMode(QSqlRelationalTableModel::LeftJoin);

    // Imposta le relazioni
    model->setRelation(professioneIdx, QSqlRelation("professione", "id", "professione"));
    model->setRelation(fonteIdx, QSqlRelation("fonte", "id", "fonte"));

    // Popola il model
    if (!model->select()) {
        showError(model->lastError());
        showError(model->query().lastQuery());
        this->doNotShow = true;
        return;
    }

    // Inizializza la combo professioni
    QSqlTableModel *professioneModel = model->relationModel(professioneIdx);
    ui->comboBoxProfessione->setModel(professioneModel);
    ui->comboBoxProfessione->setModelColumn(professioneModel->fieldIndex("professione"));

    // Inizializza la combo fonti
    QSqlTableModel *fonteModel = model->relationModel(fonteIdx);
    ui->comboBoxFonte->setModel(fonteModel);
    ui->comboBoxFonte->setModelColumn(fonteModel->fieldIndex("fonte"));

    QDataWidgetMapper *mapper = new QDataWidgetMapper(this);
    mapper->setModel(model);
    mapper->setItemDelegate(new QSqlRelationalDelegate(this));
    mapper->addMapping(ui->lineEditId, model->fieldIndex("id"));
    mapper->addMapping(ui->lineEditCognome, model->fieldIndex("cognome"));
    mapper->addMapping(ui->lineEditNome, model->fieldIndex("nome"));
    mapper->addMapping(ui->dateEditDataDiNascita, model->fieldIndex("dataNascita"));
    mapper->addMapping(ui->lineEditComuneResidenza, model->fieldIndex("comuneResidenza"));
    mapper->addMapping(ui->lineEditEmail, model->fieldIndex("email"));
    mapper->addMapping(ui->comboBoxProfessione, professioneIdx);
    mapper->addMapping(ui->comboBoxFonte, fonteIdx);

    mapper->toFirst();
}

void EditWindow::showError(const QSqlError &err)
{
    QMessageBox::critical(this, "Nicolodi",
                "Errore nell'accesso ai dati: " + err.text());
}


EditWindow::~EditWindow()
{
    delete model;
    delete ui;
}

void EditWindow::closeEvent (QCloseEvent *event)
{
    if (model->isDirty()) {
        QMessageBox::StandardButton resBtn;
        resBtn = QMessageBox::question(this
                                       , "Nicolodi"
                                       , "Dati variati: esco senza salvare?"
                                       , QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
        if (resBtn == QMessageBox::Yes) {
            event->accept();
        } else {
            event->ignore();
        }
    }
}

void EditWindow::on_pushButtonAnnulla_clicked()
{
    this->close();
}

void EditWindow::on_pushButtonSalva_clicked() {
    QMessageBox::StandardButton resBtn;
    QSqlQuery query;
    QString sql;
    int abilitaQuestionarioIdx;
    QSqlRecord currentRecord;
    if (model->isDirty()) {
        resBtn = QMessageBox::question(this
                                       , "Nicolodi"
                                       , "Dati variati: confermi salvataggio prima di uscire?"
                                       , QMessageBox::Yes | QMessageBox::No, QMessageBox::Yes);
        if (resBtn == QMessageBox::Yes) {
            abilitaQuestionarioIdx = model->fieldIndex("abilitaQuestionario");
            //currentRecord = model->record(0);
            //currentRecord.setValue(abilitaQuestionarioIdx, false);
            model->setData(model->index(0, abilitaQuestionarioIdx), false);
            // //model->query()
            // query = QSqlQuery(db);
            // sql = QString("call aggiornaQuestionarioDaNicolodi(:parId")
            //         % QString(", :parPin")
            //         % QString(", :parNome")
            //         % QString(", :parCognome")
            //         % QString(", :parDataNascita")
            //         % QString(", :parComuneResidenza")
            //         % QString(", :parEmail")
            //         % QString(", :parProfessione")
            //         % QString(", :parFonte")
            //         % QString(");");
            // query.prepare(sql);
            //
            // //query.bindValue(":parId", ui->lineEditId->text());
            // //query.bindValue(":parId", model->fieldIndex("id")->);
            model->database().transaction();
            if (model->submitAll()) {
                model->database().commit();
                this->close();
            } else {
                model->database().rollback();
                showError(model->lastError());
            }
        } else {
            this->close();
        }
    } else {
        this->close();
    }
}
