#include "status.h"

namespace Ui {

    void status(StatusType type, QString message) {
        // //QStatusBar* statusBar = mw->findChild<QStatusBar*>("statusBar");
        // QStatusBar* statusBar = mw->statusBar();
        QString prefix;
        int timeout = 0;

        // /* choose background color */
        // if (statusBar != nullptr) {
        //     switch (type) {
        //         case INFO: statusBar->setStyleSheet("color: #007;"); break;
        //         case SUCCESS: statusBar->setStyleSheet("color: #0a0;"); break;
        //         case WARNING: statusBar->setStyleSheet("color: #f50;"); break;
        //         case ERROR: statusBar->setStyleSheet("color: #d00;"); break;
        //         default: statusBar->setStyleSheet("color: #555;"); break;
        //     }
        // }
        /* choose stderr prefix */
        switch(type) {
            case INFO: prefix = "[II]"; break;
            case SUCCESS: prefix = "[OK]"; break;
            case WARNING: prefix = "[WW]"; break;
            case ERROR: prefix = "[EE]"; break;
            default: prefix = "[??]"; break;
        }
        /* choose timeout */
        switch (type) {
            case INFO: case SUCCESS: timeout = 10000; break;
            case WARNING: case ERROR: timeout = 0; break;
            default: timeout = 0; break;
        }

        /* print on stderr */
        switch (type) {
            case INFO: case SUCCESS: qDebug().noquote() << prefix << message; break;
            case WARNING: qWarning().noquote() << prefix << message; break;
            case ERROR: qCritical().noquote() << prefix << message; break;
            default: qWarning().noquote() << prefix << message; break;
        }
        // /* show message in GUI */
        // if (statusBar != nullptr) {
        //     statusBar->showMessage(message, timeout);
        //     statusBar->repaint();
        // }
    }

}
