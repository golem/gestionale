#pragma once

#include <QDebug>
#include <QSqlQuery>
#include <QSettings>
#include <QTableWidgetItem>

#include "SociListTab.h"
#include "EditWindow.h"
#include "AboutWindow.h"

namespace Ui {
    class MainWindow;
    extern QStatusBar* statusBar;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

private:
    Ui::MainWindow *ui;
    QSqlQueryModel model;

public:
    MainWindow(QWidget *parent = nullptr);

private slots:

};

