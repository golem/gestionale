#pragma once

#include <QDebug>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSettings>
#include <QTableWidgetItem>

#include "EditWindow.h"

#include "status.h"

namespace Ui {
    class SociListTab;
}

class SociListTab : public QWidget {
    Q_OBJECT

private:
    Ui::SociListTab *ui;
    QSqlQueryModel model;

public:
    SociListTab(QWidget *parent = nullptr);

private slots:
    void on_buttonCerca_clicked();
    void on_buttonNuovo_clicked();
    void on_tableSoci_clicked(const QModelIndex& index);
};
