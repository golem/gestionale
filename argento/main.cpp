#include <iostream>
#include <QApplication>
#include <QMessageBox>

#include "EditWindow.h"
#include "MainWindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    /* retrieve settings for this application, group database */
    QSettings settings("it.linux.golem", "argento");
    settings.beginGroup("database");

    /* open database and attach query */
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");  /* QMYSQL <-- database driver */
    db.setHostName(settings.value("hostname").toString());
    db.setPort(settings.value("port").toInt());
    db.setDatabaseName(settings.value("database").toString());
    db.setUserName(settings.value("username").toString());
    db.setPassword(settings.value("password").toString());

    db.open();

    MainWindow window;
    window.show();

    return a.exec();
}
