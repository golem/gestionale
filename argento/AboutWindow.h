#pragma once

#include <QDebug>
#include <QDialog>
#include <QDate>

namespace Ui {
    class AboutWindow;
}

class AboutWindow : public QDialog {
    Q_OBJECT

public:
    explicit AboutWindow(QWidget* parent = nullptr);
    ~AboutWindow();

private slots:

private:
    Ui::AboutWindow *ui;
};
