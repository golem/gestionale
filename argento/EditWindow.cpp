#include "EditWindow.h"
#include "ui_EditWindow.h"

EditWindow::EditWindow(int idSocio, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::EditWindow)
{
    ui->setupUi(this);

    model = new QSqlRelationalTableModel(this);
    model->setTable("socio");
    model->setFilter(QString("socio.id = %1").arg(idSocio));
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);

    int professioneIdx = model->fieldIndex("professione");
    int fonteIdx = model->fieldIndex("fonte");

    model->setJoinMode(QSqlRelationalTableModel::LeftJoin);

    model->setRelation(professioneIdx, QSqlRelation("professione", "id", "professione"));
    model->setRelation(fonteIdx, QSqlRelation("fonte", "id", "fonte"));

    if (! model->select()) {
        qDebug() << fullQuery(query);
        qDebug() << query.lastError().text();
        Ui::status(Ui::ERROR, "query error");
        return;
    }

    QSqlTableModel* professioneModel = model->relationModel(professioneIdx);
    ui->comboProfessione->setModel(professioneModel);
    ui->comboProfessione->setModelColumn(professioneModel->fieldIndex("professione"));

    QSqlTableModel* fonteModel = model->relationModel(fonteIdx);
    ui->comboFonte->setModel(fonteModel);
    ui->comboFonte->setModelColumn(fonteModel->fieldIndex("fonte"));

    mapper = new QDataWidgetMapper(this);
    mapper->setModel(model);
    mapper->setItemDelegate(new QSqlRelationalDelegate(this));
    mapper->addMapping(ui->lineID, model->fieldIndex("id"));
    mapper->addMapping(ui->lineCognome, model->fieldIndex("cognome"));
    mapper->addMapping(ui->lineNome, model->fieldIndex("nome"));
    mapper->addMapping(ui->dateDataNascita, model->fieldIndex("dataNascita"));
    mapper->addMapping(ui->lineComuneResidenza, model->fieldIndex("comuneResidenza"));
    mapper->addMapping(ui->lineEmail, model->fieldIndex("email"));
    mapper->addMapping(ui->comboProfessione, professioneIdx);
    mapper->addMapping(ui->comboFonte, fonteIdx);
    mapper->addMapping(ui->checkAbilitaQuestionario, model->fieldIndex("abilitaQuestionario"));
    mapper->addMapping(ui->linePin, model->fieldIndex("pin"));

    mapper->toFirst();

    QSqlQuery query;
    query.prepare("SELECT COUNT(*) AS count FROM iscrizione WHERE socio = :socio AND YEAR(anno) = :anno");
    query.bindValue(":socio", idSocio);
    query.bindValue(":anno", QDate::currentDate().year());
    if (! (query.exec() && query.next())) {
        status(Ui::ERROR, "can not get iscrizione");
    } else {
        int count = query.value(query.record().indexOf("count")).toInt();
        if (count != 0) {
            ui->buttonRinnova->setEnabled(false);
        }
    }
}

void EditWindow::on_buttonSalva_clicked() {

    if (model->isDirty()) {
        model->database().transaction();

        if (model->submitAll()) {
            model->database().commit();
            Ui::status(Ui::SUCCESS, "successfull update");
            /* update ui */
            model->select();
            mapper->toFirst();
            close();
        } else {
            model->database().rollback();
            qDebug() << model->lastError();
            Ui::status(Ui::ERROR, "model transaction submit error");
        }
    } else {
        Ui::status(Ui::INFO, "nothing done");
        close();
    }

}

void EditWindow::on_buttonAnnulla_clicked() {
    this->close();
}

void EditWindow::on_buttonRinnova_clicked() {
    int ret = QMessageBox::information(
        this,
        "Rinnovo iscrizione socio",
        QString("Confermando verrà rinnovata l'iscrizione di %1 %2 per l'anno corrente. Confermi?")
            .arg(ui->lineNome->text())
            .arg(ui->lineCognome->text()),
        QMessageBox::Cancel | QMessageBox::Ok);

    if (ret == QMessageBox::Ok) {
        qint64 id_socio = ui->lineID->text().toInt();
        QSqlQuery query;
        query.prepare("INSERT INTO iscrizione (socio) VALUES (:socio)");
        query.bindValue(":socio", id_socio);
        if (query.exec()) {
            status(Ui::SUCCESS, "registration renewal successfull");
            close();
        } else {
            status(Ui::ERROR, "registration renewal error: " + query.lastError().text());
        }
    }

}

EditWindow::~EditWindow()
{
    delete mapper;
    delete model;
    delete ui;
}
