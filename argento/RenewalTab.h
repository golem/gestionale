#pragma once

#include <QDebug>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSettings>
#include <QTableWidgetItem>

#include "status.h"

namespace Ui {
    class RenewalTab;
}

class RenewalTab : public QWidget {
    Q_OBJECT

private:
    Ui::RenewalTab *ui;
    QSqlQuery query;
    QSqlQueryModel model;

    void refresh();

public:
    RenewalTab(QWidget *parent = nullptr);

private slots:
    void on_buttonRefresh_clicked();

};
