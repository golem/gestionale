#include "MainWindow.h"
#include "ui_MainWindow.h"

namespace Ui {
    QStatusBar* statusBar = nullptr;
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    Ui::statusBar = statusBar();

    QSqlDatabase db = QSqlDatabase::database();
    if (db.isOpen()) {
        status(Ui::INFO, QString("Connected to %1 at %2").arg(db.databaseName()).arg(db.hostName()));
    } else {
        status(Ui::ERROR, "Cannot connect to database: " + db.lastError().text());
    }

    connect (ui->actionExit, &QAction::triggered, [=]() {
        close();
    });

    connect(ui->actionAbout, &QAction::triggered, [=]() {
        AboutWindow window;
        window.exec();
    });
}
