# Set C++ standards
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Qt: add dependencies to needed components
find_package(Qt5 COMPONENTS Core Widgets Sql REQUIRED)

# Qt: Automatically handle moc, .rc and .ui files.
# Search for includes in source and binary directory, so that CMake cand find
# Qt intermediate files
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# List of C++ sources for current executable
set (argento_CXXSRCS
  EditWindow.cpp
  main.cpp
  MainWindow.cpp
  status.cpp
  fullQuery.cpp
  SociListTab.cpp
  RenewalTab.cpp
  AboutWindow.cpp
  )

# Qt: list of .ui sources for current executable
set (argento_UISRCS
  MainWindow.ui
  EditWindow.ui
  SociListTab.ui
  RenewalTab.ui
  AboutWindow.ui
  )

# Qt: list of resource files
qt5_add_resources (argento_RESOURCES_RCC
  ${CMAKE_CURRENT_SOURCE_DIR}/resources.qrc
  )

# Qt: generate ui_*.h files from *.ui
qt5_wrap_ui (argento_GENUISRCS ${argento_UISRCS})
 
# Version generation: store git output in GIT_REPO_COMMIT, then generate
# version.h
execute_process(COMMAND git describe --tags --always OUTPUT_VARIABLE GIT_REPO_COMMIT)
configure_file("version.h.in" "version.h")

# Dependencies mashup to generate the executable
add_executable (argento
  ${argento_CXXSRCS}
  ${argento_GENUISRCS}
  ${argento_RESOURCES_RCC}
  )

# Qt: link needed libraries
target_link_libraries(argento Qt5::Widgets Qt5::Sql)

# CMake packaging system
install(TARGETS argento RUNTIME DESTINATION bin)
set(CPACK_PACKAGE_CONTACT "GOLEM <golem@golem.linux.it>")
include (CPack)
