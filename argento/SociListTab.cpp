#include "SociListTab.h"
#include "ui_SociListTab.h"

SociListTab::SociListTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SociListTab)
{
    ui->setupUi(this);
}

void SociListTab::on_buttonCerca_clicked() {
    QSqlQuery query;
    query.prepare("SELECT id, nome, cognome FROM socio WHERE LOWER(nome) LIKE LOWER(:nome) AND LOWER(cognome) LIKE LOWER(:cognome) ORDER BY cognome");
    query.bindValue( ":nome", QString("%%1%").arg(ui->lineNome->text()) );
    query.bindValue( ":cognome", QString("%%1%").arg(ui->lineCognome->text()) );

    if (! query.exec()) {
        qDebug() << "query not executed: " << query.lastError().text();
        status(Ui::ERROR, fullQuery(query));
        return;
    }
    if ( query.size() == 0) {
        qDebug() << "size == 0";
        status(Ui::INFO, "no results found");
        return;
    }

    model.setQuery(query);

    status(Ui::INFO, QString("%1 results found").arg(query.size()));

    ui->tableSoci->setModel(&model);
    ui->tableSoci->show();
}

void SociListTab::on_buttonNuovo_clicked() {
    QSqlQuery query;
    query.prepare("INSERT INTO socio (nome, cognome) VALUES (NULLIF(:nome, ''), NULLIF(:cognome, ''))");
    query.bindValue(":nome", ui->lineNome->text());
    query.bindValue(":cognome", ui->lineCognome->text());
    if (! query.exec()) { status(Ui::ERROR, fullQuery(query)); return; }

    if (! query.lastInsertId().isValid()) { status(Ui::ERROR, fullQuery(query)); return; }

    EditWindow*w = new EditWindow(query.lastInsertId().toInt(), this);
    w->setAttribute(Qt::WA_DeleteOnClose);
    w->show();
}

void SociListTab::on_tableSoci_clicked(const QModelIndex& index) {
    if (index.isValid()) {
        /* extracts id socio from first column of table view */
        int id = index.model()->data(index.sibling(index.row(), 0)).toInt();
        if (id != 0) {    /* if click is not on other fields than id... */
            EditWindow* w = new EditWindow(id, this);
            w->setAttribute(Qt::WA_DeleteOnClose);  /* delete window at the end */
            w->show();
        }

    }
}
