#include "RenewalTab.h"
#include "ui_RenewalTab.h"

RenewalTab::RenewalTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RenewalTab)
{
    ui->setupUi(this);

    ui->table->setModel(&model);

    QSqlQuery query;
    QSqlRecord record;

    {
        query.prepare("SELECT DISTINCT(YEAR(anno)) AS anno FROM iscrizione UNION SELECT YEAR(CURRENT_DATE()) AS anno UNION SELECT YEAR(CURRENT_DATE()) + 1 AS anno;");
        if (! query.exec()) status(Ui::ERROR, query.lastError().text());
        record = query.record();
        while (query.next()) {
            const int anno = query.value(record.indexOf("anno")).toInt();
            ui->renewalYear->addItem(QString::number(anno), anno);
        }

        int current_year;
        QDate::currentDate().getDate(&current_year, nullptr, nullptr);
        int current_year_index = ui->renewalYear->findData(current_year);
        if (current_year_index != -1) ui->renewalYear->setCurrentIndex(current_year_index);

    }


}

void RenewalTab::refresh() {
    const int anno = ui->renewalYear->currentData().toInt();

    query.prepare("SELECT YEAR(i.anno) AS anno, s.cognome, s.nome FROM iscrizione AS i INNER JOIN socio AS s ON s.id = i.socio WHERE YEAR(i.anno) = :anno ORDER BY s.cognome");
    query.bindValue(":anno", anno);

    if (! query.exec()) {
        status(Ui::ERROR, model.lastError().text());
    }

    ui->counterLabel->setText(QString("%1 soci").arg(query.size()));

    model.setQuery(query);

}

void RenewalTab::on_buttonRefresh_clicked() {
    refresh();
}