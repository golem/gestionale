#include "AboutWindow.h"
#include "ui_AboutWindow.h"

#include "version.h"

AboutWindow::AboutWindow(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::AboutWindow)
{
    ui->setupUi(this);

    ui->text->setText(
        QString("\
        Argento - Un gestionale da paura, scritto per\n\
        GOLEM - Gruppo Operativo Linux Empoli\n\
        \n\
        Questo programma è software libero rilasciato sotto GPL 3\n\
        presso git.golem.linux.it\n\
        \n\
        QT Version: %1\n\
        git commit: %2\n\
        data di compilazione: %3\n\
        ")
        .arg(QT_VERSION_STR)
        .arg(GOLEM_CURRENT_COMMIT)
        .arg(__DATE__ " " __TIME__)
    );
}

AboutWindow::~AboutWindow()
{
    delete ui;
}
