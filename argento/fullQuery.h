#ifndef FULLQUERY_H
#define FULLQUERY_H

#include <QMapIterator>
#include <QString>
#include <QSqlQuery>
#include <QVariant>

QString fullQuery(const QSqlQuery& query);

#endif