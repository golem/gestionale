#pragma once

#include <QStatusBar>
#include <QString>

#include "MainWindow.h"

namespace Ui {

    enum StatusType {
        INFO,
        SUCCESS,
        WARNING,
        ERROR
    };

    void status(StatusType type, QString message);

}

