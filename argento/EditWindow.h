#pragma once

#include <QDebug>
#include <QMainWindow>
#include <QMessageBox>
#include <QtSql>
#include <QtWidgets>

#include <iostream>

#include "EditWindow.h"
#include "fullQuery.h"
#include "status.h"

namespace Ui {
    class EditWindow;
}

class EditWindow : public QDialog {
    Q_OBJECT

public:
    explicit EditWindow(int idSocio, QWidget *parent);
    ~EditWindow();

private slots:
    void on_buttonSalva_clicked();
    void on_buttonAnnulla_clicked();
    void on_buttonRinnova_clicked();

private:
    QSqlQuery query;
    QSqlRelationalTableModel* model = nullptr;
    QDataWidgetMapper* mapper = nullptr;

    Ui::EditWindow *ui;
};
