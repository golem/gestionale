# Gestionale da Paura
A management software for GOLEM - Gruppo Operativo Linux Empoli.

## Components
* argento: president's interface to run on Sala-Corsi
* nicolodi: socio's interface to run on Limortouch

In next sections, «component» must be replaced by the proper name of the component.

## Configuration
Create `~/.config/it.linux.golem/component.conf` with following content:

    [database]
    hostname=vupiuesse.andromeda.golem.linux.it
    port=7004
    database=gestionale
    username=golem
    password=guess

## Dependencies
On Debian/Ubuntu/Mint:
* qt5-default
* TODO

# Build
    $ cd $REPOSITORY
    $ mkdir build-component
    $ cd build-component
    $ qmake ../component
    $ make -j$(nproc)

